﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace compXWpf
{
    /// <summary>
    /// Interaction logic for CEwindow.xaml
    /// </summary>
    public partial class CEwindow : Window
    {
        compWPF compX = new compWPF();
        public CEwindow()
        {
            InitializeComponent();

            this.CourseTable.ItemsSource = compX.Courses.ToList();
        }

        private void BrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            //Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            //dlg.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.* ";
            //if (dlg.ShowDialog() == true)
            //{
            //    System.IO.Stream stream = System.IO.File.Open(dlg.FileName, System.IO.FileMode.Open);

            //    BitmapImage imgsrc = new BitmapImage();
            //    imgsrc.BeginInit();
            //    imgsrc.StreamSource = stream;
            //    imgsrc.EndInit();
            //    this.Pic.Source = imgsrc;
            //}

            }

            public string GetFileNameNoExt(string FilePathFileName)
        {
            return System.IO.Path.GetFileNameWithoutExtension(FilePathFileName);
        }
    }
}
