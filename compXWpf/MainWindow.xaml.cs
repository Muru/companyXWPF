﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace compXWpf
{
   
    public partial class MainWindow : Window
    {
        compWPF compX = new compWPF();

        public MainWindow()
        {         
            InitializeComponent();
            this.InfoTable.ItemsSource = compX.Employees.ToList().Take(10);


            
        }

        
        private void Searchbtn_Click(object sender, RoutedEventArgs e)
        {
            this.InfoTable.ItemsSource = compX.Employees.Where(x => x.FullName.Contains(SearchBox.Text)).ToList();
        }

        private void SkillBtn_Click(object sender, RoutedEventArgs e)
        {
            SkillTree w3 = new SkillTree();
            w3.Show();
        }

		private void WorkerBtn_Click(object sender, RoutedEventArgs e)
		{
            AddWorkerWindow w5 = new AddWorkerWindow();
            w5.Show();
        }
		private void AddBtn_Click(object sender, RoutedEventArgs e)
		{
			Window1 w1 = new Window1();
			w1.Show();
		}

		private void CEBtn_Click(object sender, RoutedEventArgs e)
		{
            (new CEwindow()).Show();
        }

        private void HistoryBtn_Click(object sender, RoutedEventArgs e)
        {
            //HistoryWindow w2 = new HistoryWindow();
            //w2.Show();
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            compX.SaveChanges();         
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            this.InfoTable.ItemsSource = compX.Employees.Where(x => x.FullName.Contains(SearchBox.Text)).ToList();
        }

     
    }
}
